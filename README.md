# oolatoocs, a Mastodon to Bluesky bot
## A little bit of history

So what is it? Originally, I wrote, with some help, [Scootaloo](https://framagit.org/veretcle/scootaloo/) which was a Twitter to Mastodon Bot to help the [writers at NintendojoFR](https://www.nintendojo.fr) not to worry about Mastodon: the vast majority of writers were posting to Twitter, the bot scooped everything and arranged it properly for Mastodon and everything was fine and dandy. It was also used, in an altered beefed-up version, for the (now defunct) Mastodon Instance [Nupes.social](https://nupes.social) to make the tweets from the NUPES political alliance on Twitter, more easily accessible for Mastodon users.

But then Elon came, and we couldn’t read data from Twitter anymore. So we had to rely on copy/pasting things from one to another, which is not fun nor efficient.

## And now…

Hence `oolatoocs`, which takes a Mastodon Timeline and reposts it to Bluesky as properly as possible.

Since 2025-01-20, Twitter is now longer supported.

# Remarkable features

What it can do:
* Reproduces the Toot content into the Record;
* Cuts (poorly) the Toot in half in it’s too long for Bluesky and thread it (this is cut using a word count, not the best method, but it gets the job done);
* Reuploads images/gifs/videos from Mastodon to Bluesky
    * ⚠️ Bluesky does not support mixing images and videos. You can have up to 4 images on a Bsky record **or** 1 video but not mix around. If you do so, only the video will be posted on Bluesky.
    * ⚠️ Bluesky does not support images greater than 1Mb (that is 1,000,000,000 bytes or 976.6 KiB). I might incorporate soon a image quality reducer or WebP transcoding to avoid this issue.
* Can reproduce threads from Mastodon to Bluesky
* ⚠️ Bluesky does support polls for now. So the poll itself is just presented as text from Mastodon instead which is not the most elegant.
* Can prevent a Toot from being recorded to Bluesky by using the #NoTweet (case-insensitive) hashtag in Mastodon

# Configuration file

The configuration is relatively easy to follow:

```toml
[oolatoocs]
db_path = "/var/lib/oolatoocs/db.sqlite3" # the path to the DB where toots/tweets/records are stored

[mastodon] # This part can be generated, see below
base = "https://m.nintendojo.fr"
client_id = "<REDACTED>"
client_secret = "<REDACTED>"
redirect = "urn:ietf:wg:oauth:2.0:oob"
token = "<REDACTED>"

[bluesky] # this is your Bsky handle and password + a writable path for the session handling
handle = "nintendojofr.bsky.social"
password = "<REDACTED>"
config_path = "/var/lib/oolatoocs/bsky.json"
```

## How to generate the Mastodon keys?

Just run:

```bash
oolatoocs register --host https://<your-instance>
```

And follow the instructions.

## How to generate the Bluesky part?

You’ll need your handle and password. I strongly recommend a dedicated application password. You’ll also need a writable path to store the Bsky session.

# How to run

First of all, the `--help`:

```bash
A Mastodon to Twitter Bot

Usage: oolatoocs [OPTIONS] [COMMAND]

Commands:
  init      Command to init the DB
  register  Command to register to Mastodon Instance
  help      Print this message or the help of the given subcommand(s)

Options:
  -c, --config <CONFIG_FILE>  TOML config file for oolatoocs [default: /usr/local/etc/oolatoocs.toml]
  -h, --help                  Print help
  -V, --version               Print version
```

Ideally, you’ll put it an cron (from a non-root user), with the default path for config file and let it do its job. Yeah, that’s it.
