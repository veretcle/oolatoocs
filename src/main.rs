use clap::{Arg, Command};
use oolatoocs::*;

const DEFAULT_CONFIG_PATH: &str = "/usr/local/etc/oolatoocs.toml";

fn main() {
    let matches = Command::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("A Mastodon to Twitter Bot")
        .arg(
            Arg::new("config")
                .short('c')
                .long("config")
                .value_name("CONFIG_FILE")
                .help(format!("TOML config file for {}", env!("CARGO_PKG_NAME")))
                .num_args(1)
                .default_value(DEFAULT_CONFIG_PATH)
                .display_order(1),
        )
        .subcommand(
            Command::new("init")
                .version(env!("CARGO_PKG_VERSION"))
                .about("Command to init the DB")
                .arg(
                    Arg::new("config")
                        .short('c')
                        .long("config")
                        .value_name("CONFIG_FILE")
                        .help(format!("TOML config file for {}", env!("CARGO_PKG_NAME")))
                        .num_args(1)
                        .default_value(DEFAULT_CONFIG_PATH)
                        .display_order(1),
                ),
        )
        .subcommand(
            Command::new("register")
                .version(env!("CARGO_PKG_VERSION"))
                .about("Command to register to Mastodon Instance")
                .arg(
                    Arg::new("host")
                        .short('o')
                        .long("host")
                        .value_name("HOST")
                        .help(format!(
                            "Register {} to a Mastodon Instance",
                            env!("CARGO_PKG_NAME")
                        ))
                        .num_args(1)
                        .display_order(1),
                ),
        )
        .subcommand(
            Command::new("migrate")
                .version(env!("CARGO_PKG_VERSION"))
                .about("Command to register to Mastodon Instance")
                .arg(
                    Arg::new("config")
                        .short('c')
                        .long("config")
                        .value_name("CONFIG_FILE")
                        .help(format!("TOML config file for {}", env!("CARGO_PKG_NAME")))
                        .num_args(1)
                        .default_value(DEFAULT_CONFIG_PATH)
                        .display_order(1),
                ),
        )
        .get_matches();

    env_logger::init();

    match matches.subcommand() {
        Some(("init", sub_m)) => {
            let config = parse_toml(sub_m.get_one::<String>("config").unwrap());
            init_db(&config.oolatoocs.db_path).unwrap_or_else(|e| panic!("Cannot init DB: {}", e));
            return;
        }
        Some(("register", sub_m)) => {
            register(sub_m.get_one::<String>("host").unwrap());
            return;
        }
        Some(("migrate", sub_m)) => {
            let config = parse_toml(sub_m.get_one::<String>("config").unwrap());
            migrate_db(&config.oolatoocs.db_path).unwrap();
            return;
        }
        _ => (),
    }

    let config = parse_toml(matches.get_one::<String>("config").unwrap());

    run(&config);
}
