use serde::Deserialize;
use std::fs::read_to_string;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub oolatoocs: OolatoocsConfig,
    pub mastodon: MastodonConfig,
    pub bluesky: BlueskyConfig,
}

#[derive(Debug, Deserialize)]
pub struct OolatoocsConfig {
    pub db_path: String,
}

#[derive(Debug, Deserialize)]
pub struct MastodonConfig {
    pub base: String,
    pub client_id: String,
    pub client_secret: String,
    pub redirect: String,
    pub token: String,
}

#[derive(Debug, Deserialize)]
pub struct BlueskyConfig {
    pub handle: String,
    pub password: String,
    pub config_path: String,
}

/// parses TOML file into Config struct
pub fn parse_toml(toml_file: &str) -> Config {
    let toml_config =
        read_to_string(toml_file).unwrap_or_else(|e| panic!("Cannot open file {toml_file}: {e}"));

    let config: Config = toml::from_str(&toml_config)
        .unwrap_or_else(|e| panic!("Cannot parse TOML file {toml_file}: {e}"));

    config
}
