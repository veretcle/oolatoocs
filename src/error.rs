use std::{
    error::Error,
    fmt::{Display, Formatter, Result},
};

#[derive(Debug)]
pub struct OolatoocsError {
    details: String,
}

impl OolatoocsError {
    pub fn new(msg: &str) -> OolatoocsError {
        OolatoocsError {
            details: msg.to_string(),
        }
    }
}

impl Error for OolatoocsError {}

impl Display for OolatoocsError {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}", self.details)
    }
}
